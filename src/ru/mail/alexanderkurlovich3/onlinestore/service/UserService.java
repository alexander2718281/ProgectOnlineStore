package ru.mail.alexanderkurlovich3.onlinestore.service;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.util.List;

public interface UserService {
    User login(String email);
    int register(User user);
    List<User> getAllUsers();


}
