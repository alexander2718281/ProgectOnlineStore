package ru.mail.alexanderkurlovich3.onlinestore.service.impl;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.util.UserPrincipalConverter;
import ru.mail.alexanderkurlovich3.onlinestore.dao.OrderDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.connaction.ConnectionService;
import ru.mail.alexanderkurlovich3.onlinestore.dao.impl.OrderDaoImpl;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Order;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;
import ru.mail.alexanderkurlovich3.onlinestore.service.OrderService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao = new OrderDaoImpl();

    @Override
    public boolean save(long productNumber, long userID, int quantity) {
        Connection connection = ConnectionService.getConnection();
        boolean sucess = false;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                int numOfProducts = orderDao.getNumOfProductsByIds(connection, productNumber, userID);
                if (numOfProducts == 0) {
                    sucess = orderDao.save(connection, productNumber, userID, quantity);
                } else {
                    sucess = orderDao.updateNumOfProducts(connection, productNumber, userID, numOfProducts + quantity);
                }
                connection.commit();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return sucess;
    }

    @Override
    public List<Order> getOrdersByUser(UserPrincipal userPrincipal) {
        Connection connection = ConnectionService.getConnection();
        List<Order> orders = null;
        User user = UserPrincipalConverter.toUser(userPrincipal);
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                orders = orderDao.getOrdersOfUser(connection, user);
                connection.commit();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return orders;
    }

    @Override
    public void deleteById(long id) {
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                orderDao.deleteById(connection, id);
                connection.commit();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
