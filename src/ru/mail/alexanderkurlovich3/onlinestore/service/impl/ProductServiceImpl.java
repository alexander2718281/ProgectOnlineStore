package ru.mail.alexanderkurlovich3.onlinestore.service.impl;


import ru.mail.alexanderkurlovich3.onlinestore.dao.ProductDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.connaction.ConnectionService;
import ru.mail.alexanderkurlovich3.onlinestore.dao.impl.ProductDaoImpl;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.service.ProductService;
import ru.mail.alexanderkurlovich3.onlinestore.service.util.ProductConverter;
import ru.mail.alexanderkurlovich3.onlinestore.service.util.SAXParserDemo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductDao productDao = new ProductDaoImpl();

    @Override
    public boolean updateProducts(String address) {
        Connection connection = ConnectionService.getConnection();
        SAXParserDemo saxParser = new SAXParserDemo();
        List<Product> products = ProductConverter.toProductList(saxParser.saxParser(address));
        if (products != null) {
            if (connection != null) {
                try {
                    connection.setAutoCommit(false);
                    for (Product product : products) {
                        Product productFromTable = productDao.getByNumber(connection, product.getNumber());
                        if (productFromTable == null) {
                            productDao.save(connection, product);
                        } else if (product.equals(productFromTable)) {
                            continue;
                        } else {
                            productDao.update(connection, product);
                        }
                    }
                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    try {
                        connection.rollback();
                    } catch (SQLException e1) {
                        System.out.println(e1.getMessage());
                        e1.printStackTrace();
                    }
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public List<Product> getAll() {
        Connection connection = ConnectionService.getConnection();
        List<Product> products = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                products = productDao.getAll(connection);
                connection.commit();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return products;
    }

    @Override
    public boolean deleteByNumber(long number) {
        Connection connection = ConnectionService.getConnection();
        boolean succeess = false;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                succeess = productDao.deleteByNumber(connection, number);
                connection.commit();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return succeess;
    }
}
