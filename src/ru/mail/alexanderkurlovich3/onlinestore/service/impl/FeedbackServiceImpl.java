package ru.mail.alexanderkurlovich3.onlinestore.service.impl;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.util.UserPrincipalConverter;
import ru.mail.alexanderkurlovich3.onlinestore.dao.FeedbackDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.connaction.ConnectionService;
import ru.mail.alexanderkurlovich3.onlinestore.dao.impl.FeedbackDaoImpl;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Feedback;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;
import ru.mail.alexanderkurlovich3.onlinestore.service.FeedbackService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class FeedbackServiceImpl implements FeedbackService {

    private FeedbackDao feedbackDao = new FeedbackDaoImpl();

    @Override
    public boolean boFeedback(String message, UserPrincipal userPrincipal) {
        Connection connection = ConnectionService.getConnection();
        boolean success = false;
        User user = UserPrincipalConverter.toUser(userPrincipal);
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                success = feedbackDao.save(connection, user, message);
                connection.commit();
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                System.out.println(e1.getMessage());
                e1.printStackTrace();
            }
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public List<Feedback> getAll() {
        Connection connection = ConnectionService.getConnection();
        List<Feedback> feedbacks = null;
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                feedbacks = feedbackDao.getAll(connection);
                connection.commit();
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                System.out.println(e1.getMessage());
                e1.printStackTrace();
            }
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return feedbacks;
    }
}
