package ru.mail.alexanderkurlovich3.onlinestore.service.impl;

import ru.mail.alexanderkurlovich3.onlinestore.dao.UserDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.connaction.ConnectionService;
import ru.mail.alexanderkurlovich3.onlinestore.dao.impl.UserDaoImpl;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;
import ru.mail.alexanderkurlovich3.onlinestore.service.UserService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao = new UserDaoImpl();

    @Override
    public User login(String email) {
        Connection connection = ConnectionService.getConnection();
        User user = null;
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                user = userDao.getUserByEmail(connection, email);
                connection.commit();
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                System.out.println(e.getMessage());
                e1.printStackTrace();
            }
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public int register(User user) {
        Connection connection = ConnectionService.getConnection();
        int res = 0;
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                boolean isExists = userDao.isExists(connection, user.getEmail());
                if (!isExists) {
                    res = userDao.register(connection, user);
                }
                connection.commit();
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                System.out.println(e.getMessage());
                e1.printStackTrace();
            }
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<User> getAllUsers() {
        Connection connection = ConnectionService.getConnection();
        List<User> users = null;
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                users = userDao.getAllUsers(connection);
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                System.out.println(e.getMessage());
                e1.printStackTrace();
            }
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return users;
    }
}