package ru.mail.alexanderkurlovich3.onlinestore.service.model;

import java.math.BigDecimal;

public class ProductDTO {
    private long number;
    private String name;
    private String info;
    private BigDecimal price;

    public ProductDTO(long number, String name, String info, BigDecimal price) {
        this.number = number;
        this.name = name;
        this.info = info;
        this.price = price;
    }

    public ProductDTO() {
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", price=" + price +
                '}';
    }
}
