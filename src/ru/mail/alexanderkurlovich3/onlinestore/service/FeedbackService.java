package ru.mail.alexanderkurlovich3.onlinestore.service;

import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Feedback;

import java.util.List;

public interface FeedbackService {

    boolean boFeedback(String message, UserPrincipal userPrincipal);

    List<Feedback> getAll();
}
