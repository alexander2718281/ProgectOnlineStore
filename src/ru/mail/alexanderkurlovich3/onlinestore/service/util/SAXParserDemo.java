package ru.mail.alexanderkurlovich3.onlinestore.service.util;


import org.xml.sax.helpers.DefaultHandler;
import ru.mail.alexanderkurlovich3.onlinestore.service.model.ProductDTO;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.List;

public class SAXParserDemo {

    public List<ProductDTO> saxParser(String address) {
        List<ProductDTO> products = null;
        try {
            File inputFile = new File(address);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler productHandler = new ProductHandler();
            saxParser.parse(inputFile, productHandler);
            ProductHandler citedHandler =(ProductHandler) productHandler;
            products = citedHandler.getProducts();
            for (ProductDTO product : products) {
                System.out.println(product);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return products;
    }
}
