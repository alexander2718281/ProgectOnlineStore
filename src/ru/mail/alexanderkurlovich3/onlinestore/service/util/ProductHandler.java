package ru.mail.alexanderkurlovich3.onlinestore.service.util;


import javafx.util.converter.BigDecimalStringConverter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.service.model.ProductDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductHandler extends DefaultHandler {

    private boolean bname = false;
    private boolean bnumber = false;
    private boolean binfo = false;
    private boolean bprice = false;

    private ProductDTO productDTO = null;

    private  List<ProductDTO> products;

    @Override
    public void startDocument() throws SAXException {
        products = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("name")) {
            bname = true;
        } else if (qName.equalsIgnoreCase("number")) {
            bnumber = true;
        } else if (qName.equalsIgnoreCase("price")) {
            bprice = true;
        } else if (qName.equalsIgnoreCase("info")) {
            binfo = true;
        } else if (qName.equalsIgnoreCase("product")){
            productDTO = new ProductDTO();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("product")) {
            products.add(productDTO);

        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bname) {
            productDTO.setName(new String(ch, start, length));
            bname = false;
        } else if (bnumber) {
            productDTO.setNumber(Long.valueOf(new String(ch, start, length)));
            bnumber = false;
        } else if (binfo) {
            productDTO.setInfo(new String(ch, start, length));
            binfo = false;
        } else if (bprice) {
            BigDecimalStringConverter bigDecimalStringConverter = new BigDecimalStringConverter();
            productDTO.setPrice(bigDecimalStringConverter.fromString(new String(ch, start, length)));
            bprice = false;
        }
    }

    public List<ProductDTO> getProducts() {
        return products;
    }
}
