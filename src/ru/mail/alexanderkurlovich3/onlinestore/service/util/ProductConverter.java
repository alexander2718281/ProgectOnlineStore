package ru.mail.alexanderkurlovich3.onlinestore.service.util;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.service.model.ProductDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductConverter {
    public static List<Product> toProductList(List<ProductDTO> productsDTO) {
        List<Product> products = new ArrayList<>();
        if (productsDTO != null) {
            for (ProductDTO productDTO : productsDTO) {
                products.add(new Product(productDTO.getNumber(), productDTO.getName(), productDTO.getInfo(), productDTO.getPrice()));
            }
            return products;
        }
        return null;
    }
}
