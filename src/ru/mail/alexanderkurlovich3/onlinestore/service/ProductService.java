package ru.mail.alexanderkurlovich3.onlinestore.service;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;

import java.util.List;

public interface ProductService {
    boolean updateProducts(String address);
    List<Product> getAll();
    boolean deleteByNumber(long number);
}
