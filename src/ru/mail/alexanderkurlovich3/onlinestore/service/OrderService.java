package ru.mail.alexanderkurlovich3.onlinestore.service;

import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Order;

import java.util.List;

public interface OrderService {
    boolean save(long productNumber, long userID, int quantity);
    List<Order> getOrdersByUser(UserPrincipal userPrincipal);
    void deleteById(long id);
}
