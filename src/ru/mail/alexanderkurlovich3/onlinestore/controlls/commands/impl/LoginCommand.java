package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.util.UserPrincipalConverter;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;
import ru.mail.alexanderkurlovich3.onlinestore.service.UserService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginCommand implements Command {
    private UserService userService = new UserServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        System.out.println(email + " " + password);
        if (email != null && !email.equals("")) {
            User user = userService.login(email);
            if (user != null) {
                if (password.equals(user.getPassword().trim())){
                    HttpSession session = request.getSession();
                    session.setAttribute("user", UserPrincipalConverter.toUserPrincipal(user));
                    switch (user.getType()){
                        case ADMIN:
                            response.sendRedirect("/dispatcher?command=adminPage");//заменить на Enum
                            break;
                        case USER:
                            response.sendRedirect("/dispatcher?command=userPage");
                            break;
                        default:
                            return ConfigurationManager.getInstance().getProperty(ConfigurationManager.WELCOM_PAGE_PATH);
                    }
                    return null;
                }
            }
        }
        String massage = "Incorrect email or password. Please, try again";
        request.setAttribute("massage", massage);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.WELCOM_PAGE_PATH);
    }

}
