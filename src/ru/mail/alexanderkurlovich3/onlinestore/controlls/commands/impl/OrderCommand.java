package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.service.OrderService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OrderCommand implements Command {

    private OrderService orderService = new OrderServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long productNumber = Long.valueOf(request.getParameter("number"));
        long userID = ((UserPrincipal)request.getSession().getAttribute("user")).getId();
        orderService.save(productNumber, userID, 1);
        response.sendRedirect("/dispatcher?command=userPage");
        return null;
    }
}
