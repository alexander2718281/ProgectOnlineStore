package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WelcomePage implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.WELCOM_PAGE_PATH);
    }
}
