package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.service.ProductService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.ProductServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteProductCommand implements Command {

    private ProductService productService = new ProductServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long number = Long.valueOf(request.getParameter("number"));
        productService.deleteByNumber(number);
        response.sendRedirect("/dispatcher?command=showProductsForAdmin");
        return null;
    }
}
