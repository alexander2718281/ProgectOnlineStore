package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.service.ProductService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.ProductServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProductsAdminPage implements Command {

    private ProductService productService = new ProductServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Product> products = productService.getAll();
        request.setAttribute("products", products);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.PRODUCTS_FOR_ADMIN_PAGE_PATH);
    }
}
