package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Order;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.service.OrderService;
import ru.mail.alexanderkurlovich3.onlinestore.service.ProductService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.OrderServiceImpl;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.ProductServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserPage implements Command {

    private ProductService productService = new ProductServiceImpl();
    private OrderService orderService = new OrderServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        List<Product> products = productService.getAll();
        request.setAttribute("products", products);
        UserPrincipal user = (UserPrincipal) request.getSession().getAttribute("user");
        List<Order> orders = orderService.getOrdersByUser(user);
        request.setAttribute("orders", orders);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.USER_PAGE_PATH);
    }
}
