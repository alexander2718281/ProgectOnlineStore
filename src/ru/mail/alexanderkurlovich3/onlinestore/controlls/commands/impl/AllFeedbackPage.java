package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Feedback;
import ru.mail.alexanderkurlovich3.onlinestore.service.FeedbackService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.FeedbackServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AllFeedbackPage implements Command{

    private FeedbackService feedbackService = new FeedbackServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Feedback> feedbacks = feedbackService.getAll();
        request.setAttribute("feedbacks", feedbacks);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ALL_FEEDBACK_PAGE_PATH);
    }
}
