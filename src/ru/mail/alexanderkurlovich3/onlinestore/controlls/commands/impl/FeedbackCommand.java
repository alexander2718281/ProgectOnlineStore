package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.service.FeedbackService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.FeedbackServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FeedbackCommand implements Command {

    private FeedbackService feedbackService = new FeedbackServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserPrincipal user = (UserPrincipal) request.getSession().getAttribute("user");
        String message = request.getParameter("message");
        if (message != null && !message.equals("")) {
            boolean success = feedbackService.boFeedback(message, user);
            if (success) {
                response.sendRedirect("/dispatcher?command=userPage");
                return null;
            }
        }
        String error = "Something goes wrong";
        request.setAttribute("error", error);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.FEEDBACK_PAGE_PATH);
    }
}
