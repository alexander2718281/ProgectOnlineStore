package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;

import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;
import ru.mail.alexanderkurlovich3.onlinestore.service.UserService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UserRegistrationCommand implements Command {
    private UserService userService = new UserServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (email != null && !email.equals("")){
            if (password != null && !password.equals("")){
                User user = User.newBuilder().withSurename(request.getParameter("surename"))
                        .withFirstName(request.getParameter("firstName"))
                        .withSecondName(request.getParameter("secondName"))
                        .withEmail(email)
                        .withPassword(password)
                        .withTelephone(request.getParameter("telephone"))
                        .withInfo(request.getParameter("info"))
                        .withType(2).build();
                int res = userService.register(user);
                if (res == 0){
                    String massage = "This email already exists";
                    request.setAttribute("massage", massage);
                    return ConfigurationManager.getInstance().getProperty(ConfigurationManager.USER_REGISTRATION_PAGE_PATH);
                }
            }
            else {
                String massage = "Enter password";
                request.setAttribute("massage", massage);
                return ConfigurationManager.getInstance().getProperty(ConfigurationManager.USER_REGISTRATION_PAGE_PATH);
            }
        }
        else {
            String massage = "Enter email";
            request.setAttribute("massage", massage);
            return ConfigurationManager.getInstance().getProperty(ConfigurationManager.USER_REGISTRATION_PAGE_PATH);
        }
        try {
            response.sendRedirect("/dispatcher?command=login&email="+email+"&password="+password);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

