package ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.CommandEnum;
import ru.mail.alexanderkurlovich3.onlinestore.service.ProductService;
import ru.mail.alexanderkurlovich3.onlinestore.service.impl.ProductServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateProductsCommand implements Command {

    private ProductService productService = new ProductServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String address = request.getParameter("address");
        String massage = null;
        if (address == null || address.equals("")) {
            massage = "Something go's wrong, check puth";
            request.setAttribute("massage", massage);
        }
        if (productService.updateProducts(address)) {
            massage = "Products updated";
            request.setAttribute("massage", massage);
        } else {
            massage = "Something go's wrong, check puth";
            request.setAttribute("massage", massage);
        }
        String command = request.getParameter("command");
        CommandEnum commandEnum = CommandEnum.getCommandEnum(command);
        if (commandEnum == CommandEnum.INPUT_XML_PAGE) {
            return ConfigurationManager.getInstance().getProperty(ConfigurationManager.INPUT_XML_PAGE_PATH);
        }else {
            response.sendRedirect("/dispatcher?command=showProductsForAdmin&massage=" + massage);
        }
        return null;
    }
}
