package ru.mail.alexanderkurlovich3.onlinestore.controlls.util;

import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

public class UserPrincipalConverter {

    public static UserPrincipal toUserPrincipal(User user) {
        return UserPrincipal.newBuilder().withEmail(user.getEmail())
                .withId(user.getId())
                .withRole(user.getType())
                .withName(user.getFirstName()).build();
    }

    public static User toUser(UserPrincipal userPrincipal){
        return User.newBuilder().withEmail(userPrincipal.getEmail())
                .withID(userPrincipal.getId())
                .withType(userPrincipal.getRole())
                .withFirstName(userPrincipal.getName()).build();
    }
}
