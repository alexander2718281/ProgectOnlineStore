package ru.mail.alexanderkurlovich3.onlinestore.controlls.filter;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.CommandEnum;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.CommandsInMethod;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.Method;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Type;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AuthentificationFilter implements Filter {

    private static final Set<CommandEnum> USER_AVAILABLE = new HashSet<>();
    private static final Set<CommandEnum> ADMIN_AVAILABLE = new HashSet<>();
    private static final Set<CommandsInMethod> PUBLIC_COMMANDS = new HashSet<>();
    private static final String LOGIN_PATH = "/index.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("AuthenticationFilter initialized");
        USER_AVAILABLE.add(CommandEnum.USER_PAGE);
        USER_AVAILABLE.add(CommandEnum.ORDER);
        USER_AVAILABLE.add(CommandEnum.DELETE_ORDER);
        USER_AVAILABLE.add(CommandEnum.FEEDBACK_PAGE);
        USER_AVAILABLE.add(CommandEnum.DO_FEEDBACK);

        ADMIN_AVAILABLE.add(CommandEnum.ADMIN_PAGE);
        ADMIN_AVAILABLE.add(CommandEnum.SHOW_USERS);
        ADMIN_AVAILABLE.add(CommandEnum.INPUT_XML_PAGE);
        ADMIN_AVAILABLE.add(CommandEnum.UPDATE_PRODUCTS);
        ADMIN_AVAILABLE.add(CommandEnum.PRODUCTS_FOR_ADMIN_PAGE);
        ADMIN_AVAILABLE.add(CommandEnum.DELETE_PRODUCT);
        ADMIN_AVAILABLE.add(CommandEnum.ALL_FEEDBACK_PAGE);

        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.LOGIN, Method.POST));
        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.LOGIN, Method.GET));
        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.WELCOM_PAGE, Method.POST));
        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.USER_REGISTRATION_PAGE, Method.GET));
        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.USER_REGISTRATION_PAGE, Method.POST));
        PUBLIC_COMMANDS.add(new CommandsInMethod(CommandEnum.USER_REGISTRATION, Method.POST));

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        System.out.println("Authentification Filter");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        HttpSession session = req.getSession(false);
        String command = req.getParameter("command");
        System.out.println("Command = " + command);
        if (session == null) {
            defaultRequest(servletRequest, servletResponse, chain, req, res, command);
        } else {
            UserPrincipal user = (UserPrincipal) session.getAttribute("user");
            if (user == null) {
                defaultRequest(servletRequest, servletResponse, chain, req, res, command);
            } else {
                CommandEnum commandEnum = CommandEnum.getCommandEnum(command);
                Type role = user.getRole();
                switch (role) {
                    case USER:
                        if (USER_AVAILABLE.contains(commandEnum)) {
                            chain.doFilter(servletRequest, servletResponse);
                        } else {
                            session.removeAttribute("user");
                            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        }
                        break;
                    case ADMIN:
                        if (ADMIN_AVAILABLE.contains(commandEnum)) {
                            chain.doFilter(servletRequest, servletResponse);
                        } else {
                            session.removeAttribute("user");
                            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        }
                        break;
                    default:
                        session.removeAttribute("user");
                        res.sendRedirect(req.getContextPath() + LOGIN_PATH);
                        break;
                }
            }
        }

    }

    private void defaultRequest(ServletRequest request, ServletResponse response, FilterChain chain, HttpServletRequest req, HttpServletResponse res, String command) throws IOException, ServletException {
        System.out.println("Default Request");
        if (PUBLIC_COMMANDS.contains(new CommandsInMethod(CommandEnum.getCommandEnum(command), Method.getMethod(req.getMethod())))) {
            System.out.println("do filter");
            chain.doFilter(request, response);
        } else {
            System.out.println("Illegal operation");
            res.sendRedirect(req.getContextPath() + LOGIN_PATH);
        }

    }

    @Override
    public void destroy() {

    }
}
