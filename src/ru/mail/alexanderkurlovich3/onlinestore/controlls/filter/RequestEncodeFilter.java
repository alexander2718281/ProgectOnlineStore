package ru.mail.alexanderkurlovich3.onlinestore.controlls.filter;


import javax.servlet.*;
import java.io.IOException;

public class RequestEncodeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Request response encode filter has been created");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        filterChain.doFilter(servletRequest, servletResponse);
        servletResponse.setContentType("text/html; charset=UTF-8");
    }

    @Override
    public void destroy() {

    }
}
