package ru.mail.alexanderkurlovich3.onlinestore.controlls;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WelcomServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher(ConfigurationManager.getInstance().getProperty(ConfigurationManager.WELCOM_PAGE_PATH)).forward(req, resp);
    }
}
