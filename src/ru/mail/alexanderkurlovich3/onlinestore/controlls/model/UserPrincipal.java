package ru.mail.alexanderkurlovich3.onlinestore.controlls.model;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Type;

import java.io.Serializable;

public class UserPrincipal implements Serializable {

    private Long id;
    private String email;
    private Type role;
    private String name;

    private UserPrincipal(Builder builder){
        id = builder.id;
        email = builder.email;
        role = builder.role;
        name = builder.name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Type getRole() {
        return role;
    }

    public static Builder newBuilder(){
        return new Builder();
    }


    public static final class Builder{
        private Long id;
        private String email;
        private Type role;
        private String name;

        private Builder(){}

        public Builder withId(long val){
            id = val;
            return this;
        }

        public Builder withEmail(String val){
            email = val;
            return this;
        }

        public Builder withRole(Type type){
            role = type;
            return this;
        }

        public Builder withName(String val){
            name = val;
            return this;
        }

        public UserPrincipal build(){
            return new UserPrincipal(this);
        }
    }
}
