package ru.mail.alexanderkurlovich3.onlinestore.controlls.model;


public enum CommandEnum {
    LOGIN ,
    USER_REGISTRATION_PAGE,
    USER_REGISTRATION,
    USER_PAGE,
    ADMIN_PAGE,
    WELCOM_PAGE,
    SHOW_USERS,
    INPUT_XML_PAGE,
    UPDATE_PRODUCTS,
    PRODUCTS_FOR_ADMIN_PAGE,
    DELETE_PRODUCT,
    ORDER,
    DELETE_ORDER,
    FEEDBACK_PAGE,
    DO_FEEDBACK,
    ALL_FEEDBACK_PAGE;


    public static CommandEnum getCommandEnum(String str) {
        switch (str) {
            case "login":
                return LOGIN;
            case "userRegistrationPage":
                return USER_REGISTRATION_PAGE;
            case "userRegistration":
                return USER_REGISTRATION;
            case "userPage":
                return USER_PAGE;
            case "adminPage":
                return ADMIN_PAGE;
            case "welcomPage":
                return WELCOM_PAGE;
            case "showUsers":
                return SHOW_USERS;
            case "inputXMLPage":
                return INPUT_XML_PAGE;
            case "updateProducts":
                return UPDATE_PRODUCTS;
            case "showProductsForAdmin":
                return PRODUCTS_FOR_ADMIN_PAGE;
            case "deleteProduct":
                return DELETE_PRODUCT;
            case "order":
                return ORDER;
            case "deleteOrder":
                return DELETE_ORDER;
            case "feedbackPage":
                return FEEDBACK_PAGE;
            case "doFeedback":
                return DO_FEEDBACK;
            case "showFeedbacks":
                return ALL_FEEDBACK_PAGE;
            default:
                return null;
        }
    }
}
