package ru.mail.alexanderkurlovich3.onlinestore.controlls.model;

public enum Method {
    POST,
    GET;


    public static Method getMethod(String str) {
        switch (str) {
            case "POST":
                return POST;
            case "GET":
                return GET;
            default:
                return null;
        }
    }


}
