package ru.mail.alexanderkurlovich3.onlinestore.controlls.model;


public class CommandsInMethod {
    private CommandEnum command;
    private Method method;

    public CommandsInMethod(CommandEnum command, Method method) {
        this.command = command;
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandsInMethod that = (CommandsInMethod) o;

        if (command != that.command) return false;
        return method == that.method;
    }

    @Override
    public int hashCode() {
        int result = command != null ? command.hashCode() : 0;
        result = 31 * result + (method != null ? method.hashCode() : 0);
        return result;
    }
}

