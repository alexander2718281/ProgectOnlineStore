package ru.mail.alexanderkurlovich3.onlinestore.controlls;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.Command;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.commands.impl.*;
import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.CommandEnum;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DispatcherServlet extends HttpServlet{

    Map<CommandEnum, Command> commands = new HashMap<>();


    @Override
    public void init() throws ServletException {
        commands.put(CommandEnum.LOGIN, new LoginCommand());
        commands.put(CommandEnum.USER_REGISTRATION_PAGE, new UserRagistrationPage());
        commands.put(CommandEnum.USER_REGISTRATION, new UserRegistrationCommand());
        commands.put(CommandEnum.USER_PAGE, new UserPage());
        commands.put(CommandEnum.ADMIN_PAGE, new AdminPage());
        commands.put(CommandEnum.WELCOM_PAGE, new WelcomePage());
        commands.put(CommandEnum.SHOW_USERS, new ShowUsersCommand());
        commands.put(CommandEnum.INPUT_XML_PAGE, new InputXMLPage());
        commands.put(CommandEnum.UPDATE_PRODUCTS, new UpdateProductsCommand());
        commands.put(CommandEnum.PRODUCTS_FOR_ADMIN_PAGE, new ProductsAdminPage());
        commands.put(CommandEnum.DELETE_PRODUCT, new DeleteProductCommand());
        commands.put(CommandEnum.ORDER, new OrderCommand());
        commands.put(CommandEnum.DELETE_ORDER, new DeleteOrderCommand());
        commands.put(CommandEnum.FEEDBACK_PAGE, new FeedbackPage());
        commands.put(CommandEnum.DO_FEEDBACK, new FeedbackCommand());
        commands.put(CommandEnum.ALL_FEEDBACK_PAGE, new AllFeedbackPage());

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
        Command activeCommand =  commands.get(CommandEnum.getCommandEnum(command));
        if (activeCommand != null){
            String page = activeCommand.execute(req, resp);
            if (page != null){
                getServletContext().getRequestDispatcher(page).forward(req, resp);
            }
        }
        else {
            System.out.println("Command does not exists");
        }
    }
}
