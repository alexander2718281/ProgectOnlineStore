package ru.mail.alexanderkurlovich3.onlinestore.controlls;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Throwable throwable = (Throwable) req.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
        Integer statusCode = (Integer) req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        String servletName = (String) req.getAttribute(RequestDispatcher.ERROR_SERVLET_NAME);
        if (servletName == null){
            servletName = "Unknown";
        }
        String requestUri = (String) req.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
        if (requestUri == null){
            requestUri = "Unknown";
        }
        System.out.println("Error information:");
        System.out.println("The status code: " + statusCode);
        System.out.println("Servlet name: " + servletName);
        System.out.println("Exception type: " + throwable.getClass().getName());
        System.out.println("The request URI: " + requestUri);
        throwable.printStackTrace();

        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
        getServletContext().getRequestDispatcher(page).forward(req, resp);
    }

    @Override
    public void destroy() {
        System.out.println("Exception servlet destroy");
    }

    @Override
    public void init() throws ServletException {
        System.out.println("Exception servlet init");
    }
}
