package ru.mail.alexanderkurlovich3.onlinestore.config;

import java.util.ResourceBundle;

public class ConfigurationManager {
    private static ConfigurationManager maneger;

    private ResourceBundle resourceBundle;

    private static final String BUNDLE_NAME = "config";

    public static final String DATABASE_DRIVER_NAME = "database.driver.name";
    public static final String DATABASE_URL = "database.url";
    public static final String DATABASE_USERNAME = "database.username";
    public static final String DATABASE_PASSWORD = "database.password";

    public static final String ADMIN_PAGE_PATH = "admin.page.path";
    public static final String USER_PAGE_PATH = "user.page.path";
    public static final String WELCOM_PAGE_PATH = "welcom.page.path";
    public static final String USER_REGISTRATION_PAGE_PATH = "userregistration.page.path";
    public static final String PAGE_WITH_USERS_PATH = "page.with.user.path";
    public static final String INPUT_XML_PAGE_PATH = "input.xml.page.path";
    public static final String PRODUCTS_FOR_ADMIN_PAGE_PATH = "products.for.admin.page.path";
    public static final String ERROR_PAGE_PATH="error.page.path";
    public static final String FEEDBACK_PAGE_PATH="feedback.page.path";
    public static final String ALL_FEEDBACK_PAGE_PATH="all.feedback.page.path";



    private ConfigurationManager(){}

    public static ConfigurationManager getInstance(){
        if (maneger == null){
            maneger = new ConfigurationManager();
            maneger.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return maneger;
    }

    public String getProperty(String key){
        return (String) resourceBundle.getObject(key);
    }

}
