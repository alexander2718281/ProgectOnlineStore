package ru.mail.alexanderkurlovich3.onlinestore.dao;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Feedback;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.util.List;

public interface FeedbackDao {

    boolean save(Connection connection, User user, String message);

    List<Feedback> getAll(Connection connection);
}
