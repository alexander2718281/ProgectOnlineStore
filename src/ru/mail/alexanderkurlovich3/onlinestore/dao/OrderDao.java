package ru.mail.alexanderkurlovich3.onlinestore.dao;

import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Order;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.util.List;

public interface OrderDao {
    boolean save(Connection connection, long productNumber, long userID, int quantity);

    int getNumOfProductsByIds(Connection connection, long productNumber, long userID);

    boolean updateNumOfProducts(Connection connection, long productNumber, long userID, int newNum);

    List<Order> getOrdersOfUser(Connection connection, User user);

    void deleteById(Connection connection, long id);
}
