package ru.mail.alexanderkurlovich3.onlinestore.dao.model;


public class Feedback {
    private Long id;
    private User user;
    private String message;

    public Feedback(Long id, User user, String message) {
        this.id = id;
        this.user = user;
        this.message = message;
    }


    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", user=" + user +
                ", message='" + message + '\'' +
                '}';
    }
}
