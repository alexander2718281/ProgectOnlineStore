package ru.mail.alexanderkurlovich3.onlinestore.dao.model;

public enum Type {
    ADMIN,
    USER
}
