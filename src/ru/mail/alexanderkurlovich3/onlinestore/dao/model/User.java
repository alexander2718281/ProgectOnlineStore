package ru.mail.alexanderkurlovich3.onlinestore.dao.model;

public class User {
    private long id;
    private String surename;
    private String firstName;
    private String secondName;
    private String email;
    private String telephone;
    private String info;
    private String password;
    private Type type;

    private User(){}

    public Type getType() {
        return type;
    }

    public String getSurename() {
        return surename;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getInfo() {
        return info;
    }

    public String getPassword() {
        return password;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", surename='" + surename + '\'' +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", info='" + info + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                '}';
    }

    private User(UserBuilder builder){
        surename = builder.surename;
        firstName = builder.firstName;
        secondName = builder.secondName;
        email = builder.email;
        telephone = builder.telephone;
        info = builder.info;
        password = builder.password;
        type = builder.type;
        id = builder.id;
    }

    public static UserBuilder newBuilder(){
        return new UserBuilder();
    }

    public static final class UserBuilder{
        private long id;
        private String surename;
        private String firstName;
        private String secondName;
        private String email;
        private String telephone;
        private String info;
        private String password;
        private Type type;

        private UserBuilder(){}

        public UserBuilder withSurename(String val){
            surename = val;
            return this;
        }
        public UserBuilder withFirstName(String val){
            firstName = val;
            return this;
        }
        public UserBuilder withSecondName(String val){
            secondName = val;
            return this;
        }
        public UserBuilder withEmail(String val){
            email = val;
            return this;
        }
        public UserBuilder withTelephone(String val){
            telephone = val;
            return this;
        }
        public UserBuilder withInfo(String val){
            info = val;
            return this;
        }
        public UserBuilder withPassword(String val){
            password = val;
            return this;
        }
        public UserBuilder withID(long val){
            id = val;
            return this;
        }

        public  UserBuilder withType(int numeOfType){
            switch (numeOfType){
                case 1:
                    type = Type.ADMIN;
                    break;
                case 2:
                    type = Type.USER;
                    break;
                default:
                    type = Type.USER;
            }
            return this;
        }

        public UserBuilder withType(Type val){
            type = val;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}