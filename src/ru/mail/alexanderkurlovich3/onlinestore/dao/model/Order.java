package ru.mail.alexanderkurlovich3.onlinestore.dao.model;



public class Order {
    private Long id;
    private User user;
    private Product product;
    private int numOfProduct;

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Product getProduct() {
        return product;
    }

    public int getNumOfProduct() {
        return numOfProduct;
    }

    public Order(Long id, User user, Product product, int numOfProduct) {
        this.id = id;
        this.user = user;
        this.product = product;
        this.numOfProduct = numOfProduct;
    }
}
