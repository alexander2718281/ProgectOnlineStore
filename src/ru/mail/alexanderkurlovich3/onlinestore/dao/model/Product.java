package ru.mail.alexanderkurlovich3.onlinestore.dao.model;


import java.math.BigDecimal;

public class Product {
    private Long number;
    private String name;
    private String info;
    private BigDecimal price;

    public Product(Long number, String name, String info, BigDecimal price) {
        this.number = number;
        this.name = name;
        this.info = info;
        this.price = price;
    }

    public Long getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public BigDecimal getPrice() {
        return price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (number != null ? !number.equals(product.number) : product.number != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (info != null ? !info.equals(product.info) : product.info != null) return false;
        return price != null ? price.equals(product.price) : product.price == null;
    }

    @Override
    public int hashCode() {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", price=" + price +
                '}';
    }
}
