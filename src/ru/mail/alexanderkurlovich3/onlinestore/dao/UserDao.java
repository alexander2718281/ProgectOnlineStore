package ru.mail.alexanderkurlovich3.onlinestore.dao;

import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.util.List;

public interface UserDao {

    User getUserByEmail(Connection connection, String email);

    int register(Connection connection, User user);

    boolean isExists(Connection connection, String email);

    List<User> getAllUsers(Connection connection);
}
