package ru.mail.alexanderkurlovich3.onlinestore.dao;


import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;

import java.sql.Connection;
import java.util.List;

public interface ProductDao {

    Product getByNumber(Connection connection, long number);

    boolean save(Connection connection, Product product);

    boolean update(Connection connection, Product product);

    List<Product> getAll(Connection connection);

    boolean deleteByNumber(Connection connection, long number);
}
