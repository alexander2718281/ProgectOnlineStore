package ru.mail.alexanderkurlovich3.onlinestore.dao.impl;

import ru.mail.alexanderkurlovich3.onlinestore.dao.FeedbackDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Feedback;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Type;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDaoImpl implements FeedbackDao {

    @Override
    public boolean save(Connection connection, User user, String message) {
        String insert = "INSERT INTO t_feedback (user_id, message) VALUES (?, ?);";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
                preparedStatement.setLong(1, user.getId());
                preparedStatement.setString(2, message);
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }

        return success;
    }

    @Override
    public List<Feedback> getAll(Connection connection) {
        String select = "SELECT fb.id, fb.user_id, fb.message, u.email, u.firstName FROM t_feedback fb JOIN t_user u ON fb.user_id = u.id;";
        List<Feedback> feedbacks = new ArrayList<>();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        User user = User.newBuilder().withID(rs.getLong("fb.user_id"))
                                .withEmail(rs.getString("u.email"))
                                .withFirstName(rs.getString("u.firstName"))
                                .withType(Type.USER).build();
                        Feedback feedback = new Feedback(rs.getLong("fb.id"), user, rs.getString("fb.message"));
                        feedbacks.add(feedback);
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return feedbacks;
    }
}
