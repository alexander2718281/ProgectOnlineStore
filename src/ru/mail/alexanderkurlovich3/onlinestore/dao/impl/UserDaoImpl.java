package ru.mail.alexanderkurlovich3.onlinestore.dao.impl;


import ru.mail.alexanderkurlovich3.onlinestore.dao.UserDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Type;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    @Override
    public User getUserByEmail(Connection connection, String email) {
        String select = "SELECT * FROM t_user WHERE email = ?;";
        User user = null;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                preparedStatement.setString(1, email);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {
                        user = User.newBuilder().withID(rs.getInt("id"))
                                .withEmail(rs.getString("email"))
                                .withPassword(rs.getString("password"))
                                .withSurename(rs.getString("sureName"))
                                .withFirstName(rs.getString("firstName"))
                                .withSecondName(rs.getString("secondName"))
                                .withTelephone(rs.getString("telephone"))
                                .withInfo(rs.getString("info"))
                                .withType(rs.getInt("type")).build();
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return user;
    }

    @Override
    public int register(Connection connection, User user) {
        String insert = "INSERT INTO  t_user (sureName, firstName, secondName, email, telephone, info, password, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        int result = 0;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
                preparedStatement.setString(1, user.getSurename());
                preparedStatement.setString(2, user.getFirstName());
                preparedStatement.setString(3, user.getSecondName());
                preparedStatement.setString(4, user.getEmail());
                preparedStatement.setString(5, user.getTelephone());
                preparedStatement.setString(6, user.getInfo());
                preparedStatement.setString(7, user.getPassword());
                Type type = user.getType();
                int intType = 0;
                if (type == Type.ADMIN) {
                    intType = 1;
                } else {
                    intType = 2;
                }
                preparedStatement.setInt(8, intType);
                result = preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public boolean isExists(Connection connection, String email) {
        String select = "SELECT id FROM t_user WHERE email = ?;";
        boolean isExists = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                preparedStatement.setString(1, email);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {
                        isExists = true;
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return isExists;
    }

    @Override
    public List<User> getAllUsers(Connection connection) {
        String select = "SELECT * FROM t_user;";
        List<User> users = new ArrayList<>();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()){
                        User user = User.newBuilder().withID(rs.getInt("id"))
                                .withEmail(rs.getString("email"))
                                .withSurename(rs.getString("sureName"))
                                .withFirstName(rs.getString("firstName"))
                                .withSecondName(rs.getString("secondName"))
                                .withTelephone(rs.getString("telephone"))
                                .withInfo(rs.getString("info"))
                                .withType(rs.getInt("type")).build();
                        users.add(user);
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return users;
    }
}