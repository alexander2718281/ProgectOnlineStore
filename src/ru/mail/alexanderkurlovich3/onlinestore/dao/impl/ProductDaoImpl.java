package ru.mail.alexanderkurlovich3.onlinestore.dao.impl;


import ru.mail.alexanderkurlovich3.onlinestore.dao.ProductDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {

    @Override
    public Product getByNumber(Connection connection, long number) {
        String select = "SELECT * FROM t_product WHERE number = ?;";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                preparedStatement.setLong(1, number);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {
                        Product product = new Product(number, rs.getString("name"), rs.getString("info"), rs.getBigDecimal("price"));
                        return product;
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean save(Connection connection, Product product) {
        String insert = "INSERT INTO t_product(number, name, info, price) VALUES (?, ?, ?, ?);";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
                preparedStatement.setLong(1, product.getNumber());
                preparedStatement.setString(2, product.getName());
                preparedStatement.setString(3, product.getInfo());
                preparedStatement.setBigDecimal(4, product.getPrice());
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return success;
    }

    @Override
    public boolean update(Connection connection, Product product) {
        String update = "UPDATE t_product SET name = ?, info = ?, price = ? WHERE number = ?;";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(update)) {
                preparedStatement.setString(1, product.getName());
                preparedStatement.setString(2, product.getInfo());
                preparedStatement.setBigDecimal(3, product.getPrice());
                preparedStatement.setLong(4, product.getNumber());
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return success;
    }

    @Override
    public List<Product> getAll(Connection connection) {
        String select = "SELECT * FROM t_product;";
        List<Product> products = new ArrayList<>();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        Product product = new Product(rs.getLong("number"), rs.getString("name"), rs.getString("info"), rs.getBigDecimal("price"));
                        products.add(product);
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return products;
    }

    @Override
    public boolean deleteByNumber(Connection connection, long number) {
        String delete = "DELETE FROM t_product WHERE number = ?;";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
                preparedStatement.setLong(1, number);
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return success;
    }
}
