package ru.mail.alexanderkurlovich3.onlinestore.dao.impl;


import ru.mail.alexanderkurlovich3.onlinestore.controlls.model.UserPrincipal;
import ru.mail.alexanderkurlovich3.onlinestore.dao.OrderDao;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Order;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.Product;
import ru.mail.alexanderkurlovich3.onlinestore.dao.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {
    @Override
    public boolean save(Connection connection, long productNumber, long userID, int quantity) {
        String insert = "INSERT INTO t_order (user_id, product_id, num) VALUES (?, ?, ?);";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
                preparedStatement.setLong(1, userID);
                preparedStatement.setLong(2, productNumber);
                preparedStatement.setInt(3, quantity);
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return success;
    }

    @Override
    public int getNumOfProductsByIds(Connection connection, long productNumber, long userID) {
        String select = "SELECT num FROM t_order WHERE user_id = ? AND product_id = ?;";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                preparedStatement.setLong(1, userID);
                preparedStatement.setLong(2, productNumber);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {
                        return rs.getInt("num");
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public boolean updateNumOfProducts(Connection connection, long productNumber, long userID, int newNum) {
        String updae = "UPDATE t_order SET num = ? WHERE user_id = ? AND product_id = ?;";
        boolean success = false;
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(updae)) {
                preparedStatement.setInt(1, newNum);
                preparedStatement.setLong(2, userID);
                preparedStatement.setLong(3, productNumber);
                int result = preparedStatement.executeUpdate();
                if (result != 0) {
                    success = true;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return success;
    }

    @Override
    public List<Order> getOrdersOfUser(Connection connection, User user) {
        String select = "SELECT ord.id, ord.num, pr.number, pr.name, pr.info, pr.price FROM t_order ord JOIN t_product pr ON ord.product_id = pr.number WHERE user_id = ?;";
        List<Order> orders = new ArrayList<>();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(select)) {
                preparedStatement.setLong(1, user.getId());
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        Product product = new Product(rs.getLong("pr.number"), rs.getString("pr.name"), rs.getString("pr.info"), rs.getBigDecimal("pr.price"));
                        Order order = new Order(rs.getLong("ord.id"), user, product, rs.getInt("ord.num"));
                        orders.add(order);
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        return orders;
    }

    @Override
    public void deleteById(Connection connection, long id) {
        String delete = "DELETE FROM t_order WHERE id = ?;";
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
