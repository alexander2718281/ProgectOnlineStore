package ru.mail.alexanderkurlovich3.onlinestore.dao.connaction;


import ru.mail.alexanderkurlovich3.onlinestore.config.ConfigurationManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {
    private static Connection connection;

    private ConnectionService() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName(ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_DRIVER_NAME));
            } catch (ClassNotFoundException e) {
                System.out.println("Where is your MySQL JDBC Driver?");
                e.printStackTrace();
                return null;
            }
            try {
                connection = DriverManager.getConnection(ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_URL),
                        ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_USERNAME),
                        ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_PASSWORD));
            } catch (SQLException e) {
                System.out.println("Connection Failed! Check output console");
                e.printStackTrace();
                return null;
            }
        }
        return connection;
    }
}
