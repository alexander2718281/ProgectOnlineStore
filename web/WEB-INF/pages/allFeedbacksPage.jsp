<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Users</title>
</head>
<body>
<br/>
<br/>
<h1 align="center">Feedback</h1>
<div class="form-group row">
    <div class="col-sm-1"></div>
    <table class="col-sm-10 table">
        <thead class="thead-light">
        <tr>
            <th scope="col">User name</th>
            <th scope="col">User email</th>
            <th scope="col">Message</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${feedbacks}" var="feedback">
            <tr>
                <th scope="row">${feedback.user.firstName}</th>
                <td>${feedback.user.email}</td>
                <td>${feedback.message}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="col-sm-1"></div>
</div>
</body>
</html>
