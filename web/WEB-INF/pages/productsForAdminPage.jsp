<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Products</title>
</head>
<body>
<br/>
<br/>
<h1 align="center">Products</h1>
<div class="form-group row">
    <div class="col-sm-1"></div>
    <table class="col-sm-10 table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Code</th>
            <th scope="col">Name of product</th>
            <th scope="col">Info</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <th scope="row">${product.number}</th>
                <td>${product.name}</td>
                <td>${product.info}</td>
                <td>${product.price}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/dispatcher?command=deleteProduct&number=${product.number}" class="btn btn-primary" role="button" aria-pressed="true">Delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="col-sm-1"></div>
</div>

<jsp:include page="inputXMLPuthPage.jsp"/>
<jsp:include page="util/js.jsp"/>
</body>
</html>
