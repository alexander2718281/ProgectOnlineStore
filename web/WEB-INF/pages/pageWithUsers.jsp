<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Users</title>
</head>
<body>
    <h1 align="center">Users</h1>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Email</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Имя</th>
            <th scope="col">Отчество</th>
            <th scope="col">Телефон</th>
            <th scope="col">Доп. информация</th>
            <th scope="col">Роль</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${users}" var="user">
            <tr>
                <th scope="row">${user.id}</th>
                <td>${user.email}</td>
                <td>${user.surename}</td>
                <td>${user.firstName}</td>
                <td>${user.secondName}</td>
                <td>${user.telephone}</td>
                <td>${user.info}</td>
                <td>${user.type}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <jsp:include page="util/js.jsp"/>
</body>
</html>
