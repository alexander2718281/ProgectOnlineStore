<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Path page</title>
</head>
<body>
<br />
<br />
<br />
<form action="${pageContext.request.contextPath}/dispatcher?command=updateProducts" method="post">
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputAddress" class="col-sm-1 col-form-label">XML address:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="address" id="inputAddress" placeholder="XML address">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form>
<c:if test="${massage != null}">
    <div class="alert alert-danger" role="alert">
        <h4 align="center"><c:out value="${massage}"/></h4>
    </div>
</c:if>


<jsp:include page="util/js.jsp"/>
</body>
</html>
