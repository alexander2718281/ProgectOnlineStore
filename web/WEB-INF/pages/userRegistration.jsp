<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Registration</title>
</head>
<body>
<br/>
<br/>
<h2 align="center">New user registration</h2>
<br/>
<form action="${pageContext.request.contextPath}/dispatcher?command=userRegistration" method="post">
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputEmail" class="col-sm-1 col-form-label">Email</label>
        <div class="col-sm-3">
            <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputPassword" class="col-sm-1 col-form-label">Password:</label>
        <div class="col-sm-3">
            <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputSurename" class="col-sm-1 col-form-label">Surename:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="surename" id="inputSurename" placeholder="Surename">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputFirstName" class="col-sm-1 col-form-label">First Name:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="firstName" id="inputFirstName" placeholder="First Name">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputLastName" class="col-sm-1 col-form-label">Second name:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="secondName" id="inputLastName" placeholder="Second Name">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputTelephone" class="col-sm-1 col-form-label">Telephone:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="telephone" id="inputTelephone" placeholder="Telephone">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="inputInfo" class="col-sm-1 col-form-label">Info:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="info" id="inputInfo" placeholder="Some information about you">
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form>
<c:if test="${massage != null}">
    <div class="alert alert-danger" role="alert">
        <h4 align="center"><c:out value="${massage}"/></h4>
    </div>
</c:if>


<jsp:include page="util/js.jsp"/>
</body>
</html>
