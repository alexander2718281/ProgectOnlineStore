<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>User page</title>
</head>
<body>
<br/>
<h1 align="center">Products</h1>
<div class="form-group row">
    <div class="col-sm-1"></div>
    <table class="col-sm-10 table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Code</th>
            <th scope="col">Name of product</th>
            <th scope="col">Info</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <th scope="row">${product.number}</th>
                <td>${product.name}</td>
                <td>${product.info}</td>
                <td>${product.price}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/dispatcher?command=order&number=${product.number}"
                       class="btn btn-primary" role="button" aria-pressed="true">Order</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="col-sm-1"></div>
</div>
<br/>
<h1 align="center">Your orders</h1>
<div class="form-group row">
    <div class="col-sm-2"></div>
    <table class="col-sm-8 table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Order number</th>
            <th scope="col">Name of product</th>
            <th scope="col">Quantity</th>
            <th scope="col">Sum</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${orders}" var="order">
            <tr>
                <th scope="row">${order.id}</th>
                <td>${order.product.name}</td>
                <td>${order.numOfProduct}</td>
                <td>${order.product.price*order.numOfProduct}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/dispatcher?command=deleteOrder&number=${order.id}"
                       class="btn btn-primary" role="button" aria-pressed="true">Delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="col-sm-2"></div>
</div>
<p align="center"><a href="${pageContext.request.contextPath}/dispatcher?command=feedbackPage" class="col-sm-1 btn btn-primary" role="button" aria-pressed="true">Do feedback</a>
</p>

<jsp:include page="util/js.jsp"/>
</body>
</html>
