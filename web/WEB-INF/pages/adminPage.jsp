<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <jsp:include page="util/head.jsp"/>
    <title>Admin page</title>
</head>
<body>
    <br />
    <h1 align="center">Admin page</h1>
    <a href="${pageContext.request.contextPath}/dispatcher?command=showUsers" type="button" class="btn btn-warning btn-lg btn-block">Показать список пользователей</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=showProductsForAdmin" type="button" class="btn btn-warning btn-lg btn-block">Показать список товаров</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=inputXMLPage" type="button" class="btn btn-warning btn-lg btn-block">Обновить список товаров</a>
    <a href="${pageContext.request.contextPath}/dispatcher?command=showFeedbacks" type="button" class="btn btn-warning btn-lg btn-block">Показать отзывы</a>




    <jsp:include page="util/js.jsp"/>
</body>
</html>
