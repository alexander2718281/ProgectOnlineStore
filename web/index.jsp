<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
    <title>Welcome</title>
</head>
<body>
    <br />
    <br />
    <br />
    <h1 align="center">Welcome wanderer</h1>
    <br />
    <br />
    <form action="${pageContext.request.contextPath}/dispatcher?command=login" method="post">
        <div class="form-group row">
            <div class="col-sm-4"></div>
            <label for="exampleInputEmail1" class="col-sm-1 col-form-label">Email:</label>
            <input type="email" class="col-sm-3 form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <div class="col-sm-4"></div>

        </div>
        <div class="form-group row">
            <div class="col-sm-4"></div>
            <label for="exampleInputPassword1" class="col-sm-1 col-form-label">Password:</label>
            <input type="password" class="col-sm-3 form-control" name="password" id="exampleInputPassword1" placeholder="Password">
            <div class="col-sm-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-sm-4"></div>
            <button type="submit" class="col-sm-1 btn btn-primary">Login</button>
            <div class="col-sm-2"></div>
            <a href="${pageContext.request.contextPath}/dispatcher?command=userRegistrationPage" class="col-sm-1 btn btn-primary" role="button" aria-pressed="true">Registration</a>
            <div class="col-sm-4"></div>
        </div>
    </form>
    <c:if test="${massage != null}">
        <div class="alert alert-danger" role="alert">
            <h4 align="center"><c:out value="${massage}" /></h4>
        </div>
    </c:if>

<script src="/resources/js/jquery-3.3.1.slim.min.js" ></script>
<script src="/resources/js/popper.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>